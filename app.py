import datetime
import json
import singer
from botocore.vendored import requests
import auth
import xml.etree.ElementTree as ET


def main(event, context):
    if event.get('detail-type') == 'Scheduled Event':
        for source in event.get('resources'):
            if 'rule/RMARequestReport' in source:
                rma_request_report('_GET_MERCHANT_LISTINGS_DATA_')
            if 'rule/RMADetectionAndNotification' in source:
                rma_detection_and_notification('_GET_MERCHANT_LISTINGS_DATA_')

def rma_request_report(reporttype):
    for country in ['US', 'CA']:
        authObj = auth.setCountry(country)
        url = 'https://' + authObj.Domain + "/Reports/2009-01-01"
        time = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
        r = singer.send_mws_signed_request(url, {
            'AWSAccessKeyId': authObj.AWSAccessKeyId,
            'MWSAuthToken': authObj.MWSAuthToken,
            'SellerId': authObj.SellerId,
            'MarketplaceIdList.Id.1': authObj.MarketplaceId,
            'SignatureMethod': 'HmacSHA256',
            'SignatureVersion': '2',
            'Timestamp': time,
            'Version': '2009-01-01',
            'Action': 'RequestReport',
            'ReportType': reporttype,
        }, authObj)


def rma_detection_and_notification(reporttype):
    email_to_send = []
    content = []
    order = [1, 0]
    cnt = 0
    for country in ['US', 'CA']:
        authObj = auth.setCountry(country)
        url = 'https://' + authObj.Domain + "/Reports/2009-01-01"
        time = datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

        r = singer.send_mws_signed_request(url, {
            'AWSAccessKeyId': authObj.AWSAccessKeyId,
            'MWSAuthToken': authObj.MWSAuthToken,
            'SellerId': authObj.SellerId,
            'SignatureMethod': 'HmacSHA256',
            'SignatureVersion': '2',
            'Timestamp': time,
            'Version': '2009-01-01',
            'Action': 'GetReportRequestList',
            'ReportTypeList.Type.1': reporttype
            }, authObj)

        report_id_list = []
        GetReportListResponse = ET.fromstring(r)
        for GetReportListResult in GetReportListResponse:
            for ReportInfo in GetReportListResult:
                if 'ReportRequestInfo' in ReportInfo.tag:
                    for detail in ReportInfo:
                        if 'GeneratedReportId' in detail.tag and detail.text not in report_id_list:
                            report_id_list.append(detail.text)

        changed = []
        if len(report_id_list) < 1:
            print("[ERR]===== Empty report id list. =====")
        else:
            report_id = report_id_list[order[cnt]]
            print (country, report_id)
            r = singer.send_mws_signed_request(url, {
                'AWSAccessKeyId': authObj.AWSAccessKeyId,
                'MWSAuthToken': authObj.MWSAuthToken,
                'SellerId': authObj.SellerId,
                'SignatureMethod': 'HmacSHA256',
                'SignatureVersion': '2',
                'Timestamp': time,
                'Version': '2009-01-01',
                'Action': 'GetReport',
                'ReportId': report_id
            }, authObj, country)

            r = r.split('\n')
            check = 0
            for line in r:
                idxes = line.split('\t')
                if len(idxes) > 3:
                    if check == 0:
                        for i in range(4):
                            if idxes[i] == "seller-sku" or idxes[i] == "出品者SKU":
                                id = i
                        check = 1
                    else:
                        if '-RMA' in idxes[id]:
                            changed.append(idxes[id])
        if len(changed) > 0:
            content.append((country, changed))
        cnt += 1
    content_str = ""        
    for con in content:
        content_str = content_str + str(con[1]) + ' in ' + str(con[0]) + ',\n'

    if len(content_str) > 0:
        email_to_send.append({
            'Content': 'Hi,\n\nOur RMA product(s),\n' + content_str + 'has(have) been turned active.\n\n',
            'Title': "Our RMA product(s) on Amazon seems to be turned active."
        })
        print(email_to_send)

        email = os.environ.get('email_to_send', 'it@tomofun.com').split(',')

        for e in email_to_send:
            _params = {
                'Reciever': email,
                'Content': e.get('Content'),
                'Title': e.get('Title'),
        }
        _params = json.dumps(_params)
        re = requests.post(
            'https://script.google.com/macros/s/AKfycbz0hTzi9KA8-QM0KwNpIC8w-KcmIpjhn2jSa8xmpvDJbGpH73SU/exec',
            headers={'Content-Type': 'application/json; charset=utf-8'},
            data=_params)

