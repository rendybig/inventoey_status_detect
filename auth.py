class AuthObj:
    def __init__(self, json):
        self.AWSAccessKeyId = json.get('AWSAccessKeyId')
        self.MWSAuthToken = json.get('MWSAuthToken')
        self.SellerId = json.get('SellerId')
        self.secretKey = json.get('secretKey')
        self.Country = json.get('Country')
        self.MarketplaceId = json.get('MarketplaceId')
        self.ASIN = json.get('ASIN')
        self.Domain = json.get('Domain')


AMAZONE_DICT = {
    'US': 'mws.amazonservices.com',
    'CA': 'mws.amazonservices.com',
    'UK': 'mws.amazonservices.co.uk',
    'JP': 'mws.amazonservices.jp',
    'ES': 'mws.amazonservices.es',
    'DE': 'mws.amazonservices.de',
    'FR': "mws.amazonservices.fr",
    'IT': "mws.amazonservices.it",
    'AU': "mws.amazonservices.com.au",
    'MX': "mws.amazonservices.com.mx"
}


def setCountry(country):
    if country == 'US':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_US,
            'MWSAuthToken': MWSAuthToken_US,
            'SellerId': Amazon_SellerId_US,
            'secretKey': Amazon_secretKey_US,
            'MarketplaceId': MarketplaceId_US,
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'AU':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_AU,
            'MWSAuthToken': MWSAuthToken_AU,
            'SellerId': Amazon_SellerId_AU,
            'secretKey': Amazon_secretKey_AU,
            'MarketplaceId': MarketplaceId_AU,
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'JP':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_JP,
            'MWSAuthToken': MWSAuthToken_JP,
            'SellerId': Amazon_SellerId_JP,
            'secretKey': Amazon_secretKey_JP,
            'MarketplaceId': MarketplaceId_JP,
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'UK':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_UK,
            'MWSAuthToken': MWSAuthToken_UK,
            'SellerId': Amazon_SellerId_UK,
            'secretKey': Amazon_secretKey_UK,
            'MarketplaceId': MarketplaceId_UK,
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'CA':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_CA,
            'MWSAuthToken': MWSAuthToken_CA,
            'SellerId': Amazon_SellerId_CA,
            'secretKey': Amazon_secretKey_CA,
            'MarketplaceId': MarketplaceId_CA,
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'MX':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_MX,
            'MWSAuthToken': MWSAuthToken_MX,
            'SellerId': Amazon_SellerId_MX,
            'secretKey': Amazon_secretKey_MX,
            'MarketplaceId': 'A1AM78C64UM0Y8',
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'FR':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_UK,
            'MWSAuthToken': MWSAuthToken_UK,
            'SellerId': Amazon_SellerId_UK,
            'secretKey': Amazon_secretKey_UK,
            'MarketplaceId': 'A13V1IB3VIYZZH',
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'DE':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_UK,
            'MWSAuthToken': MWSAuthToken_UK,
            'SellerId': Amazon_SellerId_UK,
            'secretKey': Amazon_secretKey_UK,
            'MarketplaceId': 'A1PA6795UKMFR9',
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'IT':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_UK,
            'MWSAuthToken': MWSAuthToken_UK,
            'SellerId': Amazon_SellerId_UK,
            'secretKey': Amazon_secretKey_UK,
            'MarketplaceId': 'APJ6JRA9NG5V4',
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    elif country == 'ES':
        return AuthObj({
            'AWSAccessKeyId': AWSAccessKeyId_UK,
            'MWSAuthToken': MWSAuthToken_UK,
            'SellerId': Amazon_SellerId_UK,
            'secretKey': Amazon_secretKey_UK,
            'MarketplaceId': 'A1RKKUPIHCS9HS',
            'ASIN': 'B01FXC7JWQ',
            'Domain': AMAZONE_DICT[country],
            'Country': country
        })
    else:
        raise Exception('Unknow Country-code')


# Facebook
App_id = '650615215269505'
App_secret = 'db7e914eb9d2d80286114ffb756d6067'
Access_token = 'EAAJPuyjKpoEBAHjHXMFdXHV8n1LAIiGW1PliLzBZAqP9dHqYf7G08T2mqHItTZA8XX7Eip2VL1sEPTUC7A7xseRZAaNryHH40tvq2H1YPu9xG3wC0n668tQaxM8YDVJa170H7JFZCRYf3ZCPJc9JZBv8ikBfNieoTAQrIQZBynZCbNjZAWYdmRdFmadqn4Kow0ooZD'


# Amazon
AWSAccessKeyId_US = 'AKIAIFSWQO5XMROT3GUA'
MWSAuthToken_US = 'amzn.mws.4ea38b7b-f563-7709-4bae-87aeaEXAMPLE'
MarketplaceId_US = 'ATVPDKIKX0DER'
Amazon_SellerId_US = 'A3PVV9FMRGXZ4T'
Amazon_secretKey_US = 'cWgZ0KtHmbIvjpWPLLTJ/8paPP+S+YnJBaNYiSff'

AWSAccessKeyId_MX = 'AKIAITP42X6AQ6NTORMQ'
MWSAuthToken_MX = 'amzn.mws.aaae85a7-6e35-4c4f-fed6-bb1e53e349fb'
MarketplaceId_MX = 'A1AM78C64UM0Y8'
Amazon_SellerId_MX = 'A3U09HYLMDC517'
Amazon_secretKey_MX = 'xvY5201u9LODR2ngU0eYUPTsb+7DJpNTAGAOYeTM'

AWSAccessKeyId_AU = 'AKIAI4W2MOUIYOTBROQA'
MWSAuthToken_AU = 'amzn.mws.14b07c96-3722-97e7-dfba-46a0306a6ed6'
MarketplaceId_AU = 'A39IBJ37TRP1C6'
Amazon_SellerId_AU = 'A21QH6112OM61F'
Amazon_secretKey_AU = 'omDAG809QhXYC8rTr0HH0JImPxL7zqkZZscqr+AK'

AWSAccessKeyId_UK = 'AKIAJMUKR4O2HMLTHWBQ'
MWSAuthToken_UK = 'amzn.mws.3eae795c-6961-a4a4-6245-063acdae4b73'
MarketplaceId_UK = 'A1F83G8C2ARO7P'
Amazon_SellerId_UK = 'A3PC5W5QFD837T'
Amazon_secretKey_UK = 'YwaIOQf6pkJB4QeXRTH9lAmOOzNeDvI6dXe4PVNs'

AWSAccessKeyId_CA = 'AKIAIFSWQO5XMROT3GUA'
MWSAuthToken_CA = 'amzn.mws.4ea38b7b-f563-7709-4bae-87aeaEXAMPLE'
MarketplaceId_CA = 'A2EUQ1WTGCTBG2'
Amazon_SellerId_CA = 'A3PVV9FMRGXZ4T'
Amazon_secretKey_CA = 'cWgZ0KtHmbIvjpWPLLTJ/8paPP+S+YnJBaNYiSff'

AWSAccessKeyId_JP = 'AKIAJWDDST5BFS6IZ3JQ'
MWSAuthToken_JP = 'amzn.mws.9eac11e8-1e51-c00e-56b6-51ea4f1c9371'
MarketplaceId_JP = 'A1VC38T7YXB528'
Amazon_SellerId_JP = 'ACC4IGD4EIDJ5'
Amazon_secretKey_JP = 'L5UsROvVBG2McD/8o6NMfziA2otqUl2fET+Y3lSl'

Amazon_Query_ary = ['pet camera', 'dog camera']

# Send Email API URL
SendEmailApiUrl = 'https://script.google.com/macros/s/AKfycbz0hTzi9KA8-QM0KwNpIC8w-KcmIpjhn2jSa8xmpvDJbGpH73SU/exec'

ShopifyAuthedUrl = {
    'US': 'https://1b17995e565234d75120320342b88336:bec30348d1294e66466a72921b8be1ff@furbo-dog-camera.myshopify.com',
    'CA': 'https://07e1737b8a450f9024cf53ce063b0874:0013a2d1bdb2c64e9b54c905f1f23c2b@furbo-ca.myshopify.com',
    'TW': 'https://aa328338d43c5174bd727feeab19dcf8:45367606639944c7f61aca20e305c539@furbo.myshopify.com',
    'JP': 'https://5a23df772d61c866fc0c06f13792e7c0:e9d0b6e19f3a4deba4eb3b362cc9bd48@furbo-jp.myshopify.com',
    'UK': 'https://b42136c156650ab3409d5c511e121a41:0862b24e52f1aad008ca5ac0a5447604@furbo-uk.myshopify.com',
    'AU':'https://c40d979001039ac9c4e37e2717c19015:b5ebcd42e80c929f8c43c85f19d7a03b@furbo-global.myshopify.com',
    'EU': 'https://206098ab93acaafbb6b09c247713f4bd:e3409efc6f102ba8854d6a9ebee8cd5e@furbo-eu.myshopify.com',
    'DE': 'https://d4c02fd15b0e9c2ad948ffbe0f39185c:56e8662828ea529b67453678c099ea96@furbo-de.myshopify.com',
    'MX': 'https://1277249679330a79d6f1dcaf53c716ce:7ded4f98953268e85b5e73a8021ed3e9@furbo-mx.myshopify.com'
}

MixpanelKey = 'a61a4f5fc0ab3d15e40d278ab2e09502'
MixpanelAuthedUrl = 'https://' + MixpanelKey + '@data.mixpanel.com'

url_seller_feedback = 'https://sellercentral.amazon.com/gp/feedback-manager/view-all-feedback.html?ie=UTF8&currentPage={page}&dateRange=&descendingOrder=1&fromRating=1&pageSize=50&sortType=Date&toRating=5'
cookie_seller_feedback = 'aws-ubid-main=377-8851222-3103032; aws-account-alias=tomofunprod; sid="237ITnKAesdYX37nbo4+dA==|R9mOFO+nH6B32MeW2A9Qjpq76v4SxfBwScx6hSzPs9w="; pay-session-id=39d1bfd6c0b507bcbe9ec1f1a09ce0ad; aws-target-data=%7B%22support%22%3A%221%22%7D; s_fid=72C0F21A228CC15B-1BD6A6C857B4B6E8; regStatus=registered; x-wl-uid=1o4i7EHuHiHXr/loe13apwfUjFrIKdaMfChsj6ZaoSjZhgOGgDPl49xPb464fh9fi2AEO4PvswSfDvGpf12jgpA==; __utma=194891197.1751000380.1540959404.1540959404.1541131222.2; __utmz=194891197.1541131222.2.2.utmccn=(referral)|utmcsr=google.com.tw|utmcct=/|utmcmd=referral; session-id-time-eu=2171860454l; ubid-acbuk=261-0295466-9966833; x-acbuk="F9sQapY5AtTHduAbZzVjjDH3HIZg0mqQIIx1D0PVcdQ8AypJflfeK1jzPXtpqRm?"; aws-target-visitor-id=1540808868284-510971.22_12; _mkto_trk=id:810-GRW-452&token:_mch-amazon.com-1540353083677-15161; csrf=-1039043679; aws-priv=eyJ2IjoxLCJldSI6MCwic3QiOjB9; aws-target-static-id=1541648846621-233970; s_pers=%20s_vnum%3D1544261521892%2526vn%253D1%7C1544261521892%3B%20s_invisit%3Dtrue%7C1541671321892%3B%20s_nr%3D1541669521902-Repeat%7C1549445521902%3B; noflush_locale=en; s_vn=1572406305341%26vn%3D31; s_dslv=1542251116391; aws-userInfo=%7B%22arn%22%3A%22arn%3Aaws%3Aiam%3A%3A085645846676%3Auser%2Froy%22%2C%22alias%22%3A%22tomofunprod%22%2C%22username%22%3A%22roy%22%2C%22keybase%22%3A%22BCWol%2BEeESW0Ou84oUsBg0rk4zBJD%2FOTXkdycABKBIE%5Cu003d%22%2C%22issuer%22%3A%22http%3A%2F%2Fsignin.aws.amazon.com%2Fsignin%22%7D; AMCV_4A8581745834114C0A495E2B%40AdobeOrg=-330454231%7CMCIDTS%7C17850%7CMCMID%7C03006584603393385624610707740966418503%7CMCOPTOUT-1542282096s%7CNONE%7CMCAID%7CNONE%7CvVersion%7C3.1.2; s_nr=1542274896775-Repeat; s_lv=1542274896777; at-main=Atza|IwEBIES66gHCtuRBUMKTKHgINiqW2gO7VISTScCL3itcRVvmkGM62wsKzvxm5BMgnWHE8p9vs5uAHbdGQBu2cQ4K1KxMA9Z9afUclCQeuN9sIvEOYa69NKAT8pSbePBsgiuymr0k6qMJ5zhhqrxpm2OmCCpzsk_Wdx3umGoqOqrX47piVkSSds4dpk6G_HXgz1fS7YIjUf4r4SMbJrb8oQH7-kAd-urbOxxWas227bYUE-1lKT5VO6sEPArEiYF1wZwPZOZwai2mpxE3Zy4ADccDzm9F8nxChs_3-LwWmSTezjlg-qVz3QXhS8yIYGKQWtRuLuFksEwdSD27l_62wGdT_Xn5eTVKFE1IYagWiOZkZnYmC6UUZHdg67jIDDqQpHWeuoS-y2dAjh1jnbg6tT_hA1t_ZU3SA4z3-Fs3hsePBGPMRfQABYNhsv4_vdkI402XDHZrzQxmHkxhX6bqaNR06RQ1MH-eMO2S5J6c7Q2U9cLPfHpDMmCjM0mFtBsScUxRSyQ; sess-at-main="plauuTPfl0D/gxfR6LJO0wtMPXmBwMyfzVRMwJ1GIXE="; sst-main=Sst1|PQEHQoYpwsduhdXMd_PYpQQSEM2TP7anmTv8MqwA5CfXqQ_I6vjlxp1MqjYX_bNNccHkkpHMpCVAU-oj1ZLcq6o-ev6WzLRYt1xwjRjSqn_2Pqzo6UQbLaHdTCiUILJ5V_ZyPB1tHnnldKkjat7g3vCP-zOoH_GUu2aA4ZD3tsJn4XTSs0HZzFgoxrkibL9oAOx7PnyZ6Wm2YiusESVipAVdSfjQe-omZ5TeYYr0EQHZ1SBwjBkmd6FD3WrJlwp-76gbRxamO8_5JtBHG4lW8USRKR2YCInDl2TwHyUc8cZquIBCjG8cotf2WC2ctQt_xabmTriz4Q1Y-3KdShoNgIiC-A20iE0s-dDHhj6-IVSRtgIs7qQJCzCTDRI_xBJqW0bhcuHqNtZX4LFI3fGo28tSW-8lInlRvPpuQBUMZRLPpM8IIxdb457sNCR07B7mrbWZ; JSESSIONID=D53CB1C0442CE2C509546D8984F1A173; at-acbjp=Atza|IwEBIFktM-KK7HrZPlujyKDwnmeQP8FjjWY16IJlfo4nczyvb0GDhF36oK1o6A105bkgk1TX4oLFI-WsBy8AjHw6zfDqUaMzTkeEvTJtH10yOQX4dRXiTaCf--64LeZeyvBfMpGPahhKzkHfBFSsOBVJsOLN5iC9gdilr2s3yA2fmng-VJ0s8oz5avZ901-2Lem-tYFmFpYgvtwcfZFaZgtY_D6R6vfq9o53fVzYYqTZkPQ5tazyDTmSTUZ7cing2VnHcBkA7ZpwjGowrroel1uXNvvJcu0p8HhMIBFO7DUeBI2njSMsyNmwzyn0thX5YHglOqke4cqtQwWhKm5ZfsLZ6C15DHkLSrtqwPmHKElwMzE8-8cJ4xazcyK3exutnVf795pYCoS1Mb3jowa6C69jTqn4; sess-at-acbjp="FQGYNlnamZ+IhP9q9Z/g33SWDUwT5/4ivDenZCRbJ+Q="; session-id-time-jp=1542873600l; session-id-jp=357-8429013-5181268; ubid-acbjp=356-7243696-5510558; x-acbjp="r7vkmbD2a7CiQR?rqbCBVWTJ88XCxTp6iPX@sBl7XnspIZidhmK7KWvGB7Q8M4fA"; session-id-time=2082787201l; csm-hit=tb:s-354XRW65A0CC1XDPG841|1542347070587&adb:adblk_yes&t:1542163547689; session-id=138-8528039-9915317; ubid-main=134-2150735-9352744; session-token="bILM0LxH6RCapzngppp5uAzsWhN+lf7Yt99r65kVaeinn6ucUhBmH1eNY0XBwKvJC8DurMfztPCvxAwiOW13s5U5oB5CEmvmoRtpx/XSpL8W0TwdthIwgWb5Dl6e/IdPyzYOP2RlV3LI6c1vyJ1Jo/JaYiO2u+up8m5fPBE7P+5Y6DCjxZM6IQ7HpObNDR0CyId+K2HDDxYtkZTn+O9bupscG4neQ+T8MvqeYudNJBw="; x-main="Ou2?Vq9DiXkUS2gUfWxH2@cVrs2@QCPJFxqAXOZp9h6EjJjywa5YtMnLF2gSPRyn"'
