# -*- coding: utf-8 -*-
from botocore.vendored import requests
import base64
import hashlib
import hmac
import urllib.parse


def send_mws_signed_request(url, obj, authObj, country='US'):
    arr = url.replace('https://', 'POST\n').split('/')
    tosign = arr[0] + '\n'
    i = 1
    while i < len(arr):
        tosign = tosign + '/' + arr[i]
        i = i + 1
    tosign = tosign + '\n'

    var = []
    for key in obj:
        var.append(key)
    var.sort()

    isFirst = 1
    i = 0
    while i < len(var):
        if isFirst == 1:
            isFirst = 0
        else:
            tosign += '&'
        tosign += var[i] + '=' + urllib.parse.quote_plus(obj[var[i]])
        i = i + 1

    dig = hmac.new(str.encode(authObj.secretKey),
                   msg=tosign.encode('utf-8'),
                   digestmod=hashlib.sha256).digest()

    sig = base64.b64encode(dig).decode()
    sig = urllib.parse.quote_plus('=').join(sig.split('='))
    sig = urllib.parse.quote_plus('+').join(sig.split('+'))
    sig = urllib.parse.quote_plus('/').join(sig.split('/'))

    obj['Signature'] = sig

    url_params = ''

    isFirst = 1
    for key in obj:
        if isFirst == 1:
            isFirst = 0
        else:
            url_params += '&'
        if key != 'Signature':
            url_params += key + '=' + urllib.parse.quote_plus(obj[key])
        else:
            url_params += key + '=' + obj[key]

    r = requests.post(url,
                      headers={'Content-Type': 'application/x-www-form-urlencoded'},
                      data=url_params)

    if country == 'JP':
        r.encoding = 'cp932'
    else:
        r.encoding = 'utf-8'

    return r.text
